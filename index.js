#!/usr/bin/env node

const program = require('commander')
const chalk = require('chalk')
const clipboardy = require('clipboardy')
const createPassword = require('./utils/createPassword')
const savePassword = require('./utils/savePassword')

program.version('1.0.0').description('simple password generator')

program
    .option('-l, --lenght <number>', 'lenght of password', '8')
    .option('-s, --save', 'save the password to passwords.txt')
    .option('-nn, -no-numbers', 'remove numbers')
    .option('-ns, -no-symbols', 'remove symbols')
    .parse()

const { lenght, save, numbers, symbols } = program.opts()

const generatedPassword = createPassword(lenght, numbers, symbols)

if (save) {
    savePassword(generatedPassword)
}
clipboardy.writeSync(generatedPassword)

console.log(chalk.blue('Generated Password: ') + chalk.bold(generatedPassword))
console.log(chalk.yellow('Coppied to clipboard'))