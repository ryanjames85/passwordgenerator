# A Simple Password Generator.

This is a simple password generator program that can make a randomly generated password to any given lenght and character choice.

To use the program with defualt configurations just run.

- `npm run start`

If you want to run the script with arguemnts run.

- ` node index.js -<>`

```
Options:
  -V, --version          output the version number
  -l, --lenght <number>  lenght of password (default: "8")
  -s, --save             save the password to passwords.txt
  -nn, -no-numbers       remove numbers
  -ns, -no-symbols       remove symbols
  -h, --help             display help for command
```